﻿// task_2_Fracts.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
#include <cmath>
using namespace std;

class Rationals{
	int numerator; //числитель
	int denomenator; //знаменатель
	int get_gcd(int a, int b) { //функция поиска НОД
		if (b == 0)
			return a;
		return get_gcd(b, a % b);
	}
	void simplify() {  //функция упрощения дроби
		if (denomenator < 0) { //перетаскиваем знак в числитель
			numerator *= -1;
			denomenator *= -1;
		}
		if (abs(numerator) < 2) { //если изначально у нас +-1 в числителе, выводим на печать
			cout << numerator << "/" << denomenator << endl;
		}
		int gcd = get_gcd(abs(numerator), denomenator); //ищем НОД и упрощаем дробь
		numerator /= gcd;
		denomenator /= gcd;
	}
public: //опишем свойства класса
//пропишем геттеры и сеттеры числителя и знаменателя
	int get_numerator() { return numerator; }
	void set_numerator(int numer) { numerator = numer; }

	int get_denomenator() { return denomenator; }
	void set_denomenator(int denom) { denomenator = denom; }

	Rationals() { numerator = 0; denomenator = 1; } //пустой конструктор
	Rationals(int value) : numerator(value), denomenator(1) {} //конструктор для ввода одного числа
	Rationals(int numerator, int denomenator) : numerator(numerator), denomenator(denomenator) { //создали конструктор для двух значений
		if (this->denomenator == 0) throw logic_error("Division by zero."); //выходим, если деление на ноль
		simplify(); //если все норм, применим упрощение дроби
	}
//пропишем теперь операторы арифметики
	Rationals operator-(Rationals & a) {
		return Rationals(-a.get_numerator(), a.get_denomenator());
	}

	Rationals operator+(Rationals& a, Rationals& b) {
		int common_denom = a.get_denomenator * b.get_denomenator;
		int common_num = a.get_numerator * b.get_numerator;
		return Fraction(common_num, common_denom);
	}
	


}





int main()
{
    std::cout << "Hello World!\n";
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
