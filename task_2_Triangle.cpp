﻿// task_2_Classes.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>
class Point{
public:
	float x, y;
	Point() {}
	Point(float x, float y) {
		this->x = x;
		this->y = y;
	}
	
};

class Triangle {
private:
	Point a; Point b; Point c;
public:
	Triangle(Point a, Point b, Point c): a(a), b(b), c(c) {};
	float get_distance() {							//на мой взгляд, расстояние между точками в котексте задачи требуется
		float distance = abs(sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2))); //лишь для вычисления площади и периметра, а поскольку они
		return distance;													//являются методами класса, то и get_distance целесообразно реализовать внутри класса
	}
	

	float get_perimeter() {
		float w = get_distance(a, b);
		float u = get_distance(b, c);
		float v = get_distance(c, a);

		float P = w + u + v;
		return P;
	}
	float get_area() {
		float s = abs((a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y))/2;
		return s;
	}

};





int main()
{
  
	Point p(0.0, 0.0), q(0.0, 1.0), r(1.0, 0.0);
	Triangle t(p, q, r);
	std::cout<<t.get_perimeter(p,q,r)<<std::endl;
	std::cout << t.get_area(p,q,r) << std::endl;
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
